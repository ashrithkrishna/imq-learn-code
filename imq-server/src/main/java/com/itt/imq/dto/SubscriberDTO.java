package com.itt.imq.dto;

public class SubscriberDTO 
{
	private int id;
	private String subscriberId;
	private int portNumber;
	private String subscriberIP;
	private String data;
	private int topicId;
	
	public int getTopicId() {
		return topicId;
	}
	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}
	private String dataPullTimeStamp;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public int getPortNumber() {
		return portNumber;
	}
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	public String getSubscriberIP() {
		return subscriberIP;
	}
	public void setSubscriberIP(String subscriberIP) {
		this.subscriberIP = subscriberIP;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDataPullTimeStamp() {
		return dataPullTimeStamp;
	}
	public void setDataPullTimeStamp(String dataPullTimeStamp) {
		this.dataPullTimeStamp = dataPullTimeStamp;
	}
}
