package com.itt.imq.dto;

public class PublisherDTO 
{
	private int id;
	private String publisherId;
	private int portNumber;
	private String publisherIP;
	private String data;
	private String datasendTimeStamp;
	private String connectedTime;
	private int topicId;
	
	public int getTopicId() {
		return topicId;
	}
	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}
	public String getConnectedTime() {
		return connectedTime;
	}
	public void setConnectedTime(String connectedTime) {
		this.connectedTime = connectedTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClientId() {
		return publisherId;
	}
	public void setClientId(String clientId) {
		this.publisherId = clientId;
	}
	public int getPortNumber() {
		return portNumber;
	}
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	public String getClientIP() {
		return publisherIP;
	}
	public void setClientIP(String clientIP) {
		this.publisherIP = clientIP;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDatasendTimeStamp() {
		return datasendTimeStamp;
	}
	public void setDatasendTimeStamp(String datasendTimeStamp) {
		this.datasendTimeStamp = datasendTimeStamp;
	}
}
