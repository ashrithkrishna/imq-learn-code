package com.itt.imq.queue;

import java.util.ArrayDeque;
import java.util.Deque;

public class QueueManager 
{
	private static final Deque<String> deque = new ArrayDeque<String>(); 
	
	private static QueueManager single_instance = null; 
	
	private QueueManager()
	{
	}
	
	public static QueueManager getInstance() 
    { 
        if (single_instance == null) 
        	single_instance = new QueueManager(); 
  
        return single_instance; 
    } 
	public void addToQueue(String value)
	{
		QueueManager.deque.add(value);
		System.out.println("Data Added to Queue");
	}
	
	public int getQueueSize()
	{
		System.out.println(deque.size());
		return QueueManager.deque.size();
	}
	
	public String popFromQueue()
	{
		if(deque.isEmpty())
		{
			return "No message in the queue";
		}
		else
		{
			return deque.pop();
		}
	}
}
