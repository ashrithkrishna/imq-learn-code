package com.itt.imq.queue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.itt.imq.database.DatabaseManager;
import com.itt.imq.util.Constants;

public class QueueCleaner extends Thread
{
	static DatabaseManager databaseManager = DatabaseManager.getInstance();
	static Date date;
	
	public void run()
	{
		ResultSet queueData;
		Thread.currentThread();
		
		while(true)
		{
			try 
			{
				queueData = getQueueData();
				
				traverseAndCleanQueue(queueData);
			
				Thread.sleep(10000);
			}
			catch (SQLException e) 
			{
				System.out.println("SQL Exception : "+e.getMessage());
			} 
			catch (InterruptedException e) 
			{
				System.out.println("Interrupted Exception : "+e.getMessage());
			} 
			catch (ParseException e) 
			{
				System.out.println("Parse Exception : "+e.getMessage());
			}
		}
    }

	private static void traverseAndCleanQueue(ResultSet queueData) throws SQLException, ParseException 
	{
		if (!queueData.next()) 
		{
		    System.out.println("Queue Cleaner : No data in the queue");
		} 
		else 
		{
		    do 
		    {	
				boolean validity = checkValidity(queueData.getString("timestamp"));
				if(validity==false || queueData.getString("data").equalsIgnoreCase("Test data"))
				{
					removeFromQueue(queueData);
				}
		    } 
		    while (queueData.next());
		}
	}

	private static void removeFromQueue(ResultSet queueData) throws SQLException 
	{
		System.out.println("Removing the element : "+queueData.getString("data")+"  "+queueData.getString("timestamp"));
		
		if(!queueData.getString("data").equalsIgnoreCase("Test data"))
		{
			databaseManager.addToDeadLetterQueue(queueData.getString("data"));
		}
		databaseManager.removeFromQueue(queueData.getString("data"),queueData.getString("timestamp"));
	}

	private static boolean checkValidity(String timestamp) throws ParseException 
	{
		 Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timestamp);

		 long duration  = new Date().getTime() - date1.getTime();
		 long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
		 
		 if(diffInMinutes>Constants.MAX_TIME_IN_QUEUE)
		 {
			 System.out.println("Difference in minutes : "+diffInMinutes);
			 return false;
		 }
		 else
		 {
			 return true;
		 } 
	}

	private static ResultSet getQueueData() 
	{
		return databaseManager.getQueueData();
	}
}