package com.itt.imq.util;

public final class Constants {

	private Constants(){}
	
	public static final int SERVER_PUBLISHER_PORT=5056;
	
	public static final int SERVER_SUBSCRIBER_PORT=5057;
	
	public static final int MAX_NO_OF_CLIENTS = 2;
	
	public static final int MAX_TIME_IN_QUEUE = 5;
	
	public static final String DB_CONNECTION_URL = "jdbc:mysql://localhost:3306/imq-ashrith";
	
	public static final String DB_USERNAME = "root";
	
	public static final String DB_PASSWORD = "root";
	
	public static final String SUCCESS_MESSAGE = "Success";
	
}
