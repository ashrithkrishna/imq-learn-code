package com.itt.imq.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.itt.imq.database.DatabaseManager;

public class TopicManager 
{	
	public String getAvailableTopics() throws InterruptedException 
	{
		ResultSet allTopics = DatabaseManager.getInstance().getAllTopics();
		
		String allTopicsString = "";
		
		try 
		{
			if (!allTopics.next()) 
			{
			    return "No topics!";
			} 
			else 
			{
				do
			    {
			    	String temp = allTopics.getInt("topic_id")+" "+allTopics.getString("topic_name");
			    	allTopicsString = allTopicsString+temp+"\n";
			    } 
			    while (allTopics.next());
			}
		} 
		catch (SQLException e) 
		{
			return "SQLException in fetching topics."+e.getMessage();
		}
		return allTopicsString;
	}
	
	public void addTopic(String topicName)
	{
		DatabaseManager.getInstance().addTopic(topicName);
	}
}
