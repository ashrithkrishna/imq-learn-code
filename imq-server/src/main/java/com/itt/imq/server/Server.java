package com.itt.imq.server;

import java.net.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.itt.imq.queue.QueueCleaner;
import com.itt.imq.util.Constants;

import java.io.*; 

public class Server 
{ 
	public static int CURRENT_NO_OF_PUBLISHERS=0;
	public static int CURRENT_NO_OF_SUBSCRIBERS=0;
	public static boolean wait=false;
	public static Deque<String> deque = new ArrayDeque<String>();
	
	public static void main(String[] args) throws IOException  
    { 
		@SuppressWarnings("resource")
        final ServerSocket serverPublisherSocket = new ServerSocket(Constants.SERVER_PUBLISHER_PORT);
        System.out.println("Server for Publisher Started. Listening on port 5056.");
        
        @SuppressWarnings("resource")
		final ServerSocket serverSubscriberSocket = new ServerSocket(Constants.SERVER_SUBSCRIBER_PORT);
        System.out.println("Server for Subscriber Started. Listening on port 5057.");
        
        startQueueCleaner();
        
        startClientManagers(serverPublisherSocket, serverSubscriberSocket);
    }

	private static void startQueueCleaner() 
	{
		Thread queueCleanerThread = new QueueCleaner(); 
        queueCleanerThread.start();   
        System.out.println("\nQueue cleaner Started");
	}

	private static void startClientManagers(final ServerSocket serverPublisherSocket, final ServerSocket serverSubscriberSocket) 
	{
		Callable<Void> callable1 = new Callable<Void>()
        {
           public Void call() throws Exception
           {
        	  handlePublishers(serverPublisherSocket);
              return null;
           }
        };
        
        Callable<Void> callable2 = new Callable<Void>()
        {
           public Void call() throws Exception
           {
        	  handleSubscribers(serverSubscriberSocket);
              return null;
           }
        };
        
        List<Callable<Void>> taskList = new ArrayList<Callable<Void>>();
        taskList.add(callable1);
        taskList.add(callable2);
        
        ExecutorService executor = Executors.newFixedThreadPool(2);

        try
        {
           executor.invokeAll(taskList);
        }
        catch (InterruptedException ie)
        {
           System.out.println("Interrupped exception occurred : "+ie.getMessage());
        }	
	}

	private static void handlePublishers(ServerSocket serverPublisherSocket) throws IOException 
	{
		System.out.println("Server ready for Publisher");
		while (true)  
        { 
            Socket socket = null; 	// socket object to receive incoming client requests 
            try 
            { 
            	if(Server.CURRENT_NO_OF_PUBLISHERS < Constants.MAX_NO_OF_CLIENTS)
            	{
            		socket = serverPublisherSocket.accept(); 
                    Server.CURRENT_NO_OF_PUBLISHERS++;

                    System.out.println("\nA new publisher is connected : " + socket); 
                    System.out.println("Current number of publisher/s = " + Server.CURRENT_NO_OF_PUBLISHERS);
                    
                    // create a new thread object for each publisher
                    Thread thread = new PublisherManager(socket); 
                    thread.start();   
            	}
            	else
            	{
            		Thread.currentThread();
					Thread.sleep(2000);
					continue;
            	}
            } 
            catch (Exception exception)
            { 
            	socket.close(); 
            	exception.printStackTrace(); 
            } 
        } 
	} 
	
	private static void handleSubscribers(ServerSocket serverSubscriberSocket) throws IOException
	{
		System.out.println("Server ready for subscribers");
		
		while (true)  
        { 
            Socket socket = null; 	// socket object to receive incoming client requests 
            try 
            { 
            	if(Server.CURRENT_NO_OF_SUBSCRIBERS < Constants.MAX_NO_OF_CLIENTS)
            	{
            		socket = serverSubscriberSocket.accept(); 
                    Server.CURRENT_NO_OF_SUBSCRIBERS++;

                    System.out.println("\nA new subscriber is connected : " + socket); 
                    System.out.println("Current number of subscriber/s = " + Server.CURRENT_NO_OF_SUBSCRIBERS);
                    
                    // create a new thread object for each Subscriber
                    Thread thread = new SubscriberManager(socket); 
                    thread.start();   
            	}
            	else
            	{
            		Thread.currentThread();
					Thread.sleep(2000);
					continue;
            	}
            } 
            catch (Exception exception)
            { 
            	socket.close(); 
            	exception.printStackTrace(); 
            } 
        } 
	}
} 