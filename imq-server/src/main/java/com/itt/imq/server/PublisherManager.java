package com.itt.imq.server;

import java.io.DataInputStream;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import com.itt.imq.database.DatabaseManager;
import com.itt.imq.dto.PublisherDTO;
import com.itt.imq.util.JSONManager;
import com.itt.imq.util.TopicManager;

public class PublisherManager extends Thread
{
    final DataInputStream dis; 
    final DataOutputStream dos; 
    final Socket socket; 
    final int publisherPort;
    final InetAddress inetAddress;
    
    DatabaseManager databaseManager = DatabaseManager.getInstance();
    TopicManager topicManager = new TopicManager();
    
    PublisherDTO publisherDTO= new PublisherDTO();
    
    public PublisherManager(Socket socket) throws IOException  
    { 
        this.socket = socket; 
        this.dis = new DataInputStream(socket.getInputStream());
		this.dos = new DataOutputStream(socket.getOutputStream()); 
		this.publisherPort = socket.getPort();
		this.inetAddress = socket.getInetAddress();
    } 
  
    @Override
    public void run()  
    { 
        Thread.currentThread();
        Date date = new Date();
		String connectedTime = new Timestamp(date.getTime()).toString();
        String receivedJsonString; 
        String receivedMessage;
        int topicId=0;
        
		try 
		{
			while(true)
			{
				dos.writeUTF("Available topics :\n"+topicManager.getAvailableTopics()+"Select the topic for which data has to be pushed."+"\n\nEnter 0 to add new topic."+"\nEnter 'Bye' or 'Exit' to exit.");
				receivedJsonString = dis.readUTF();
				receivedMessage = JSONManager.getMessageFromJson(receivedJsonString);
				
				if(receivedMessage.equalsIgnoreCase("Exit") || receivedMessage.equalsIgnoreCase("Bye")) 
                {  
                	closeConnection(this.socket);
                	break;
                } 
				
				else if(validateUserInput(receivedMessage) == true || Integer.parseInt(receivedMessage)==0)
				{
					topicId = Integer.parseInt(receivedMessage);
					
					if(topicId == 0)
					{
						dos.writeUTF("\nEnter topic name :");
						topicManager.addTopic(JSONManager.getMessageFromJson(dis.readUTF()));
						
						continue;
					}
					else
					{
						while(true)
				        {
							boolean flag = manageDataAdditionToQueue(receivedJsonString,receivedMessage,topicId,connectedTime);
							if(flag == false)
							{
								break;
							}
				        }
					}
				}
				else
				{
					dos.writeUTF("Invalid choice\nPress any key to continue");
					dis.readUTF();
					continue;
				}
			}
		} 
		catch (IOException e1) 
		{
			System.out.println("Publisher on "+publisherPort+" disconnected abruptly");
        	Server.CURRENT_NO_OF_PUBLISHERS--;
        	System.out.println("Current number of publisher/s : "+Server.CURRENT_NO_OF_PUBLISHERS);
		} 
		catch (SQLException e2) 
		{
			System.out.println("An SQL exception occurred while Validating User input.\n Exception : "+e2.getMessage());
		} 
		catch (InterruptedException e) 
		{
			System.out.println(e.getMessage());
		}
    }

	private boolean manageDataAdditionToQueue(String receivedJsonString, String receivedMessage, int topicId, String connectedTime) throws IOException 
	{
		dos.writeUTF("\nServer : Tell me Mr.Publisher on port number : "+publisherPort+" Inet : "+inetAddress+"\nPress 'Back' to go back"); 
        
        // receive the answer from client 
        receivedJsonString = dis.readUTF();
        receivedMessage = JSONManager.getMessageFromJson(receivedJsonString);
        
        if(receivedMessage.equals("Back")) 
        {  
        	return false;
        } 
        
        databaseManager.addtoQueue(receivedJsonString, topicId);
        
        prepareClient(receivedMessage,connectedTime,topicId);
        
        databaseManager.addtoPublisherDB(publisherDTO);
        return true;
	}

	private boolean validateUserInput(String receivedMessage) throws SQLException 
	{
		ResultSet allTopicIds = databaseManager.getAllTopicIds();
		
		int userInput = Integer.parseInt(receivedMessage);
		
		if (!allTopicIds.next()) 
		{
		    return false;
		} 
		else 
		{
			do
		    {
		    	int temp = allTopicIds.getInt("topic_id");
		    	if(temp == userInput)
		    		return true;
		    } 
		    while (allTopicIds.next());
			
			return false;
		}
	}

	private void closeConnection(Socket socket) throws IOException 
	{
        System.out.println("\nPublisher on " + this.socket + " wants to close connection"); 
        System.out.println("Closing this connection."); 
        socket.close();
        Server.CURRENT_NO_OF_PUBLISHERS--;
        System.out.println("Connection closed!\nCurrent number of nPublisher = "+Server.CURRENT_NO_OF_PUBLISHERS+"\n");
	}
	
	private void prepareClient(String received, String connectedTime, int topicId)
	{
		publisherDTO.setClientId(inetAddress.toString().substring(1)+"P"+publisherPort+"T"+connectedTime);
		publisherDTO.setPortNumber(publisherPort);
		publisherDTO.setClientIP(inetAddress.toString().substring(1));
		publisherDTO.setData(received);
		publisherDTO.setTopicId(topicId);
		publisherDTO.setConnectedTime(connectedTime);
	}
}