package com.itt.imq.server;

import java.io.DataInputStream;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Date;

import com.itt.imq.database.DatabaseManager;
import com.itt.imq.dto.SubscriberDTO;
import com.itt.imq.util.TopicManager;

public class SubscriberManager extends Thread
{
    final DataInputStream dis; 
    final DataOutputStream dos; 
    final Socket socket; 
    final int clientPort;
    final InetAddress inetAddress;
    
    DatabaseManager databaseManager = DatabaseManager.getInstance();
    SubscriberDTO subscriberDTO = new SubscriberDTO();
    Date date = new Date();
    String subscriberConnectedTime;
    
    public SubscriberManager(Socket socket) throws IOException 
    {
    	this.socket = socket; 
        this.dis = new DataInputStream(socket.getInputStream());
		this.dos = new DataOutputStream(socket.getOutputStream()); 
		this.clientPort = socket.getPort();
		this.inetAddress = socket.getInetAddress();
		this.subscriberConnectedTime = new Timestamp(date.getTime()).toString();
	}
	@Override
    public void run()  
	{
		TopicManager topicManager = new TopicManager();
		
		while(true)
		{
			try 
			{
				dos.writeUTF("Available topics :\n"+topicManager.getAvailableTopics()+"Select the topic for which data has to be pulled."+"\nEnter -1 to exit");
				System.out.println();
				int topicId = Integer.parseInt(dis.readUTF());
				
				if(topicId==-1)
				{
					closeConnection(this.socket);
                	break;
				}
				
				if(databaseManager.getQueueSize(topicId)<1)
				{
					dos.writeUTF("No data in queue!\nPress any key to continue or -1 to exit");
					String userInput = dis.readUTF();
					
					if(Integer.parseInt(userInput)==-1)
					{
						closeConnection(this.socket);
	                	break;
					}
					continue;
				}
				else
				{
					dos.writeUTF("\nCurrent Queue size : "+databaseManager.getQueueSize(topicId)+"\nPull now? Y/N");
					String userInput = dis.readUTF();
					
					if(userInput.equalsIgnoreCase("Y"))
					{
						manageQueuePull(topicId);	
					}
				}
			} 
			catch (IOException e) 
			{
				System.out.println("Subscriber on "+clientPort+" disconnected abruptly");
            	Server.CURRENT_NO_OF_SUBSCRIBERS--;
            	System.out.println("Current number of subscriber/s : "+Server.CURRENT_NO_OF_SUBSCRIBERS);
                break;
			} 
			catch (InterruptedException e) 
			{
				System.out.println("Interrupted Exception : "+e.getMessage());
			}
		}
	} 
	
	private void manageQueuePull(int topicId) throws IOException 
	{
		String userInput;
		
		while(true)
		{
			if(databaseManager.getQueueSize(topicId)>0)
			{
				String pulledData = databaseManager.popQueue(topicId);
				dos.writeUTF("Pulled element : "+pulledData+"\nQueue size now : "+databaseManager.getQueueSize(topicId)+"\nPress any key");
				prepareSubscriber(subscriberDTO, pulledData, topicId);
				databaseManager.addToSubscriberDb(subscriberDTO);
				dis.readUTF();
			}
			else
			{
				dos.writeUTF("Cannot pull element as the queue is empty\nClick ok");
				dis.readUTF();
				break;
			}
			
			dos.writeUTF("Pull again? Y/N");
			userInput = dis.readUTF();
			
			if(userInput.equalsIgnoreCase("Y"))
			{
				continue;
			}
			else
			{
				break;
			}
		}
	}
	
	private void prepareSubscriber(SubscriberDTO subscriberDTO, String dataPulled, int topicId)
	{
		subscriberDTO.setSubscriberId(inetAddress.toString().substring(1)+"P"+clientPort+"T"+subscriberConnectedTime);
		subscriberDTO.setPortNumber(clientPort);
		subscriberDTO.setSubscriberIP(inetAddress.toString().substring(1));
		subscriberDTO.setTopicId(topicId);
		subscriberDTO.setData(dataPulled);
	}
	
	private void closeConnection(Socket socket) throws IOException 
	{
        System.out.println("\nClient " + this.socket + " wants to close connection"); 
        System.out.println("Closing this connection."); 
        socket.close();
        Server.CURRENT_NO_OF_SUBSCRIBERS--;
        System.out.println("Connection closed!\nCurrent number of subscribers = "+Server.CURRENT_NO_OF_SUBSCRIBERS+"\n");
	}
}