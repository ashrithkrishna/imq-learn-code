package com.itt.imq.database;

import java.sql.*;
import java.util.Date;

import com.itt.imq.dto.PublisherDTO;
import com.itt.imq.dto.SubscriberDTO;
import com.itt.imq.encryption.Encryptions;
import com.itt.imq.util.Constants;

public class DatabaseManager 
{
	static Connection connection;
	
	private static DatabaseManager single_instance = null; 
	
	PreparedStatement preparedStatement;
	
	private DatabaseManager()
	{
		getDBConnection();
		
		if(connection != null)
		{
			System.out.println("Database connection successful");
		}
	}
	
	public static Connection getDBConnection()
	{
		try 
		{
			connection=DriverManager.getConnection(Constants.DB_CONNECTION_URL,Constants.DB_USERNAME,Constants.DB_PASSWORD);
			return connection;
		} 
		catch (SQLException e) 
		{
			try 
			{
				connection=DriverManager.getConnection(Constants.DB_CONNECTION_URL,Constants.DB_USERNAME,Constants.DB_PASSWORD);
				return connection;
			} 
			catch (SQLException e1) 
			{
				System.out.println("An exception occurred while creating connection with Database. Please check you DB credentials and try again.");
				System.out.println("Current DB username : "+Constants.DB_USERNAME+"\nConnection URL : "+Constants.DB_CONNECTION_URL);
				return null;
			}
		}
	}
	
	public static DatabaseManager getInstance() 
    { 
        if (single_instance == null) 
        	single_instance = new DatabaseManager(); 
  
        return single_instance; 
    } 
	
	public String addtoPublisherDB(PublisherDTO publisherDTO)
	{
		Date date = new Date();
		try 
		{
			preparedStatement = connection.prepareStatement("insert into publisher(publisherId,portNumber,publisherIP,data,topic_id,datasendTimeStamp,connectedTime) values(?,?,?,?,?,?,?)");
			preparedStatement.setString(1, publisherDTO.getClientId());
			preparedStatement.setInt(2, publisherDTO.getPortNumber());
			preparedStatement.setString(3, publisherDTO.getClientIP());
			preparedStatement.setString(4, Encryptions.encrypt(publisherDTO.getData()));
			preparedStatement.setInt(5, publisherDTO.getTopicId());
			preparedStatement.setString(6, new Timestamp(date.getTime()).toString());
			preparedStatement.setString(7, publisherDTO.getConnectedTime());
			
			preparedStatement.executeUpdate();
			
			return Constants.SUCCESS_MESSAGE;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.out.println("An SQL exception occurred while inserting data into root table.");
			return e.getMessage();
		} 
		catch (Exception e) 
		{
			System.out.println("An Exception occurred while encryting data. "+e.getMessage());
			return e.getMessage();
		} 
	}
	
	public String addtoQueue(String data, int chosenTopic)
	{
		PreparedStatement preparedStatement;
		Date date = new Date();
		try 
		{
			preparedStatement = connection.prepareStatement("insert into queue(data,timestamp,topic_id) values(?,?,?)");
			preparedStatement.setString(1, data);
			preparedStatement.setString(2, new Timestamp(date.getTime()).toString());
			preparedStatement.setInt(3, chosenTopic);
			preparedStatement.executeUpdate();
			
			return Constants.SUCCESS_MESSAGE;
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while inserting data into the queue "+chosenTopic);
			return e.getMessage();
			
		} 
		catch (Exception e) 
		{
			return e.getMessage();
		}  
	}
	
	public String popQueue(int topicId)
	{
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `imq-ashrith`.queue WHERE topic_id=? LIMIT 1;");
			preparedStatement.setInt(1, topicId);
			ResultSet rs = preparedStatement.executeQuery();
			rs.next();
			String poppedElement = rs.getString(1);

			preparedStatement = connection.prepareStatement("DELETE FROM `imq-ashrith`.queue WHERE topic_id=? LIMIT 1;");
			preparedStatement.setInt(1, topicId);
			
			preparedStatement.executeUpdate();
			
			return poppedElement;
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while popping data from the queue with topic Id : "+topicId);
		}
		return null;
	}
	
	public int getQueueSize(int topicId)
	{
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS rowcount FROM queue WHERE topic_id=?");
			preparedStatement.setInt(1, topicId);
			
			ResultSet rs = preparedStatement.executeQuery();
			rs.next();
			int count = rs.getInt("rowcount") ;
			rs.close() ;
			return count;
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while getting the queue Size. Please try again.");
			return -1;
		}  
	}
	
	public ResultSet getQueueData()
	{
		try 
		{
			Statement statement = connection.createStatement();
			ResultSet rs=statement.executeQuery("SELECT * FROM queue;");
			return rs;
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while getting the queue Data. Please try again.");
		}
		return null;
	}

	public void removeFromQueue(String data, String timestamp) 
	{
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `imq-ashrith`.queue WHERE data=? AND timestamp=?");
			preparedStatement.setString(1, data);
			preparedStatement.setString(2, timestamp);
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while removing data from the queue.");
		}
	}
	
	public void addToDeadLetterQueue(String data) 
	{
		PreparedStatement preparedStatement;
		Date date = new Date();
		try 
		{
			preparedStatement = connection.prepareStatement("insert into dead_letter_queue(data,timestamp) values(?,?)");
			preparedStatement.setString(1, data);
			preparedStatement.setString(2, new Timestamp(date.getTime()).toString());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while adding data to the dead letter queue.");
		}
	}
	
	public ResultSet getAllTopics()
	{
		try 
		{
			Statement statement = connection.createStatement();
			ResultSet rs=statement.executeQuery("SELECT * FROM topics;");
			return rs;
		} 
		catch (SQLException e) 
		{
			System.out.println("SQL Exception : Unable to get all topics.");
			return null;
		}
	}

	public ResultSet getAllTopicIds() 
	{
		try 
		{
			Statement statement = connection.createStatement();
			ResultSet rs=statement.executeQuery("SELECT topic_id FROM topics;");
			return rs;
		} 
		catch (SQLException e) 
		{
			System.out.println("SQL Exception : Unable to get topics IDs.");
			return null;
		}
	}
	
	public void addTopic(String topicName)
	{
		PreparedStatement preparedStatement;
		
		try 
		{
			int previousTopicId = getPreviousTopicId();
			preparedStatement = connection.prepareStatement("insert into topics(topic_id,topic_name) values(?,?)");
			preparedStatement.setInt(1, previousTopicId+1);
			preparedStatement.setString(2, topicName);
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e) 
		{
			System.out.println("SQL Exception : Unable to add topic. Please try again after sometime."+"\n"+e.getMessage());
		}
	}

	private int getPreviousTopicId() throws SQLException 
	{
		Statement statement = connection.createStatement();
		ResultSet rs=statement.executeQuery("SELECT MAX(topic_id) FROM `imq-ashrith`.topics;");
		rs.next();
		return rs.getInt("MAX(topic_id)");
	}

	public String addToSubscriberDb(SubscriberDTO subscriberDTO) 
	{
		Date date = new Date();
		try 
		{
			preparedStatement = connection.prepareStatement("insert into subscriber(subscriberId,portNumber,subscriberIP,data_pulled,topic_id,datapullTimeStamp) values(?,?,?,?,?,?)");
			preparedStatement.setString(1, subscriberDTO.getSubscriberId());
			preparedStatement.setInt(2, subscriberDTO.getPortNumber());
			preparedStatement.setString(3, subscriberDTO.getSubscriberIP());
			preparedStatement.setString(4, Encryptions.encrypt(subscriberDTO.getData()));
			preparedStatement.setInt(5, subscriberDTO.getTopicId());
			preparedStatement.setString(6, new Timestamp(date.getTime()).toString());
			
			preparedStatement.executeUpdate();
			
			return Constants.SUCCESS_MESSAGE;
		} 
		catch (SQLException e) 
		{
			System.out.println("An SQL exception occurred while adding data into subscriber table.");
			return e.getMessage();
		} 
		catch (Exception e) 
		{
			System.out.println("An Exception occurred while encryting data. "+e.getMessage());
			return e.getMessage();
		} 
	}
}