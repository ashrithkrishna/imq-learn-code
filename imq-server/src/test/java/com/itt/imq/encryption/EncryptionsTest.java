package com.itt.imq.encryption;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EncryptionsTest {

	@Test
	void testEncrypt() throws Exception 
	{
		String testString = "TestString";
		assertNotEquals(testString, Encryptions.encrypt(testString));
	}
}
