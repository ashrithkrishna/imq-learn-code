package com.itt.imq.database;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.itt.imq.dto.PublisherDTO;

class DatabaseManagerTest {
	
	@Test
	void testGetDBConnection() 
	{
		assertNotEquals(null, DatabaseManager.getDBConnection(), "DB connection should not be null");
	}
	
	@Test
	void testAddtoDB() 
	{
		PublisherDTO publisherDTO = Mockito.mock(PublisherDTO.class);
		
		Mockito.when(publisherDTO.getClientId()).thenReturn("TestClientId");
		Mockito.when(publisherDTO.getPortNumber()).thenReturn(0000);
		Mockito.when(publisherDTO.getClientIP()).thenReturn("0.0.0.0");
		Mockito.when(publisherDTO.getData()).thenReturn("Test data");
		Mockito.when(publisherDTO.getDatasendTimeStamp()).thenReturn("12:00:00");
		Mockito.when(publisherDTO.getConnectedTime()).thenReturn("12:00:00");
		Mockito.when(publisherDTO.getTopicId()).thenReturn(1);
		
		assertEquals("Success", DatabaseManager.getInstance().addtoPublisherDB(publisherDTO), DatabaseManager.getInstance().addtoPublisherDB(publisherDTO));
	}
	
	@Test
	void testAddtoQueue() 
	{
		assertEquals("Success", DatabaseManager.getInstance().addtoQueue("Test data", 1), DatabaseManager.getInstance().addtoQueue("Test data", 1));
	}
	
	@Test
	void testGetQueueSize() 
	{
		assertNotEquals(-1,DatabaseManager.getInstance().getQueueSize(1));
	}
	
	@Test
	void testGetAllTopics() 
	{
		assertNotEquals(null, DatabaseManager.getInstance().getAllTopics());
	}

	@Test
	void testGetAllTopicIds() 
	{
		assertNotEquals(null, DatabaseManager.getInstance().getAllTopicIds());
	}
}
