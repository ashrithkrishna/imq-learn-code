package com.itt.imq.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TopicManagerTest {

	@Test
	void testGetAvailableTopics() 
	{
		TopicManager topicManager = Mockito.mock(TopicManager.class);
		
		try 
		{
			topicManager.getAvailableTopics();
		} 
		catch (InterruptedException e) 
		{
			fail(e.getMessage());
		}
	}
}
