package com.itt.imq.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

public class Subscriber 
{
	static DataInputStream dis = null;
	static DataOutputStream dos = null;
	
	public static void main(String[] args) throws IOException 
	{
		Scanner scanner = new Scanner(System.in);
		
		try
		{
			String tosendMessage;
			// getting localhost ip 
			InetAddress ip = InetAddress.getByName("localhost"); 
			
			System.out.println("Connecting to server...");
			// establish the connection with server port 5057 
			Socket socket = new Socket(ip, 5057);
			
			if(socket.isConnected()==true)
			{
				dis = new DataInputStream(socket.getInputStream()); 
				dos = new DataOutputStream(socket.getOutputStream()); 
				
				while (true) 
				{ 
					System.out.println(dis.readUTF()); 
					tosendMessage = scanner.nextLine(); 
					
					dos.writeUTF(tosendMessage);
					
					if(tosendMessage.equals("-1")) 
					{ 
						closeConnection(socket);
						break; 
					} 
				}
			}
			dis.close(); 
			dos.close(); 
		}
		catch(ConnectException e)
		{ 
			System.out.println("Problem in connecting to the server. Please try after sometime.");
		}
		catch(SocketException e)
		{
			System.out.println("Server unreachable! Please try to connect back.\nException : "+e.getMessage());
		}
		finally 
		{
			scanner.close(); 
		}
	}
	
	private static void closeConnection(Socket socket)
	{
		System.out.println("Closing this connection : " + socket); 
		try 
		{
			socket.close();
			System.out.println("Connection closed"); 
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		} 
	} 
}
