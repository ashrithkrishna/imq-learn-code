package com.itt.imq.client;

import java.io.*; 

import java.net.*;
import java.util.Scanner;

import org.json.JSONObject;

import com.itt.imq.util.JSONManager; 

public class Publisher 
{ 
	static DataInputStream dis = null;
	static DataOutputStream dos = null;
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) throws IOException 
	{
		try
		{ 
			// getting localhost ip 
			InetAddress ip = InetAddress.getByName("localhost"); 
			
			System.out.println("Connecting to server...");
			// establish the connection with server port 5056 
			Socket socket = new Socket(ip, 5056); 

			if(socket.isConnected()==true)
			{
				startClientServerInteraction(socket);
				
			}
			dis.close(); 
			dos.close();
		}
		catch(ConnectException e)
		{ 
			System.out.println("Problem in connecting to the server. Please try after sometime.");
		} 
		catch(SocketException e)
		{
			System.out.println("Server unreachable! Please try to connect back.\nException : "+e.getMessage());
		}
		finally 
		{
			scanner.close(); 
		}
	}

	private static void startClientServerInteraction(Socket socket) throws IOException 
	{
		String tosendMessage;
		JSONObject json;
		
		// obtaining input and out streams 
		dis = new DataInputStream(socket.getInputStream()); 
		dos = new DataOutputStream(socket.getOutputStream()); 
		
		// the following loop performs the exchange of information between client and clientManager
		while (true) 
		{ 
			System.out.println(dis.readUTF()); 
			tosendMessage = scanner.nextLine(); 
			
			json = JSONManager.getJSON(tosendMessage);
			dos.writeUTF(json.toString()); 
			
			if(tosendMessage.equals("Exit") || tosendMessage.equals("Bye")) 
			{ 
				closeConnection(socket);
				break; 
			} 
		} 
	}

	private static void closeConnection(Socket socket)
	{
		System.out.println("Closing this connection : " + socket); 
		try 
		{
			socket.close();
			System.out.println("Connection closed"); 
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
	} 
} 